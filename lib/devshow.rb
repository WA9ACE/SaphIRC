require 'open-uri'

class DevShow
  include Cinch::Plugin
  
  timer 5, method: :live?
  timer 300, method: :nick_check
  
  match /simon_says (.+)/, method: :simon_says

  def live?
    live_url = 'http://live.devshow.co/stream'
    #if !stream = open(live_url) { |f| f.read }
    #  Channel("#uberbot").send "The Dev Show is not live."
    #else
    #  Channel("#uberbot").send "The Dev Show is live!"
    #end
  end
  
  def nick_check
    if @bot.nick == "UberBot" or @bot.nick == "UberBot-dev"
      puts "Nick is fine, no change necessary."
    else
      puts "Fixing nickname."
      @bot.nick = "UberBot"
    end
  end
  
  def simon_says(m)
    tmp = m.message.split(" ")
    channel = tmp[1]
    2.times do
      tmp.delete_at(0)
    end
    message = tmp.join(" ")
    Channel(channel).send(message)
    m.reply message
    m.reply channel
  end
end