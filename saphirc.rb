$LOAD_PATH << File.join(Dir.getwd, 'lib')

require 'cinch'
require 'sinatra'
require 'admin'
require 'google'
require 'devshow'
require 'urban'
require 'twitter_broadcast'

$bot = Cinch::Bot.new do
  configure do |c|
    c.server = "irc.freenode.net"
    c.nick   = "UberBot"
    c.channels = ["#uberbot", "#sgs2-att"]
    c.plugins.plugins = [Admin, Google, DevShow, UrbanDictionary, TwitterBroadcast]
  end
end

Thread.new do
  $bot.start
end

get '/' do
  erb :index, :layout => :layout
end